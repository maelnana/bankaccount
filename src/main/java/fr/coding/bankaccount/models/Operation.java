package fr.coding.bankaccount.models;

import fr.coding.bankaccount.exceptions.AmountNegativeException;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Operation {
    private final Long accountID;
    private final BigDecimal amount;
    private final Instant time;
    private final OperationType operationType;

    private Operation(Long accountID, BigDecimal amount, OperationType operationType, Instant time) {
        this.accountID = accountID;
        this.amount = amount;
        this.operationType = operationType;
        this.time = time;
    }

    public static Operation create(Long accountID, BigDecimal amount, OperationType operationType, Instant time) throws AmountNegativeException {
        validateNotNull(accountID, amount, operationType, time);

        if (amount.compareTo(BigDecimal.ZERO) < 0)
            throw new AmountNegativeException(amount);

        return new Operation(accountID, amount, operationType, time);
    }

    private static void validateNotNull(Long accountID, BigDecimal amount, OperationType operationType, Instant time) {
        List<String> illegalArguments = new ArrayList<>();

        if (accountID == null)
            illegalArguments.add("accountID");

        if (amount == null)
            illegalArguments.add("amount");

        if (operationType == null)
            illegalArguments.add("operationType");

        if (time == null)
            illegalArguments.add("time");

        if (!illegalArguments.isEmpty()) {
            throw new IllegalArgumentException(String.join(",", illegalArguments));
        }
    }

    public Long getAccountID() {
        return accountID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Instant getTime() {
        return time;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(accountID, operation.accountID) &&
                Objects.equals(amount, operation.amount) &&
                Objects.equals(time, operation.time) &&
                operationType == operation.operationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, amount, time, operationType);
    }
}
