package fr.coding.bankaccount.services;

import fr.coding.bankaccount.exceptions.AccountNotFoundException;
import fr.coding.bankaccount.exceptions.AmountNegativeException;
import fr.coding.bankaccount.exceptions.NotEnoughMoneyOnAccountException;
import fr.coding.bankaccount.models.Operation;
import fr.coding.bankaccount.models.OperationType;
import fr.coding.bankaccount.printers.OperationPrinter;
import fr.coding.bankaccount.repositories.OperationRepository;

import java.math.BigDecimal;
import java.util.List;

public class AccountService {
    private final OperationRepository operationRepository;
    private final DateService dateService;

    public AccountService(OperationRepository operationRepository, DateService dateService) {
        this.operationRepository = operationRepository;
        this.dateService = dateService;
    }

    public void deposit(Long accountID, BigDecimal amount) throws AmountNegativeException, AccountNotFoundException {
        Operation depositOperation = Operation.create(accountID, amount, OperationType.DEPOSIT, dateService.getDate());
        operationRepository.add(depositOperation);
    }

    public void withdraw(Long accountID, BigDecimal amount) throws AmountNegativeException, NotEnoughMoneyOnAccountException, AccountNotFoundException {
        Operation withdrawalOperation = Operation.create(accountID, amount, OperationType.WITHDRAWAL, dateService.getDate());
        List<Operation> operations = operationRepository.findAll(accountID);
        BigDecimal balance = computeBalance(operations);

        if (balance.compareTo(amount) < 0) {
            throw new NotEnoughMoneyOnAccountException();
        }
        operationRepository.add(withdrawalOperation);
    }

    public void printStatement(OperationPrinter operationPrinter, Long accountID) throws AccountNotFoundException {
        List<Operation> operations = operationRepository.findAll(accountID);
        operationPrinter.print(operations, computeBalance(operations));
    }

    private BigDecimal computeBalance(List<Operation> operations) {
        BigDecimal currentBalance = BigDecimal.ZERO;

        for (Operation operation : operations) {
            BigDecimal amount = operation.getAmount();
            switch (operation.getOperationType()) {
                case WITHDRAWAL:
                    currentBalance = currentBalance.subtract(amount);
                    break;
                case DEPOSIT:
                    currentBalance = currentBalance.add(amount);
                    break;
                default:
                    break;
            }
        }
        return currentBalance;
    }
}
