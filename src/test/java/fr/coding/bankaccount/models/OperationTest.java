package fr.coding.bankaccount.models;

import fr.coding.bankaccount.exceptions.AmountNegativeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OperationTest {
    @ParameterizedTest
    @MethodSource("provideOperations")
    void should_validate_operation_creation(Long accountID, BigDecimal amount, OperationType operationType, Instant date) throws AmountNegativeException {
        Operation operation = Operation.create(accountID, amount, operationType, date);

        assertAll(() -> {
            assertEquals(accountID, operation.getAccountID());
            assertEquals(amount, operation.getAmount());
            assertEquals(operationType, operation.getOperationType());
            assertEquals(date, operation.getTime());
        });
    }

    private static Stream<Arguments> provideOperations() {
        return Stream.of(
                Arguments.of(123L, BigDecimal.ZERO, OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802)),
                Arguments.of(54L, BigDecimal.ONE, OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802)),
                Arguments.of(78L, BigDecimal.TEN, OperationType.WITHDRAWAL, Instant.ofEpochSecond(1424080802)),
                Arguments.of(795L, new BigDecimal(366), OperationType.WITHDRAWAL, Instant.ofEpochSecond(1424080802)),
                Arguments.of(645L, new BigDecimal(6546), OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802))
        );
    }

    @ParameterizedTest
    @MethodSource("provideOperationsForEquals")
    void should_validate_equals_operations(Operation o1, Operation o2) {
        assertEquals(o1, o2);
    }

    private static Stream<Arguments> provideOperationsForEquals() throws AmountNegativeException {
        Instant d1 = Instant.ofEpochSecond(1424080802);
        Instant d2 = Instant.ofEpochSecond(1455616802);
        Instant d3 = Instant.ofEpochSecond(1487239202);
        Instant d4 = Instant.ofEpochSecond(1518775202);

        return Stream.of(
                Arguments.of(
                        Operation.create(123L, BigDecimal.ZERO, OperationType.DEPOSIT, d1),
                        Operation.create(123L, BigDecimal.ZERO, OperationType.DEPOSIT, d1)
                ),
                Arguments.of(
                        Operation.create(56L, BigDecimal.ONE, OperationType.DEPOSIT, d2),
                        Operation.create(56L, BigDecimal.ONE, OperationType.DEPOSIT, d2)
                ),
                Arguments.of(
                        Operation.create(865L, BigDecimal.TEN, OperationType.WITHDRAWAL, d3),
                        Operation.create(865L, BigDecimal.TEN, OperationType.WITHDRAWAL, d3)
                ),
                Arguments.of(
                        Operation.create(40984L, BigDecimal.ZERO, OperationType.DEPOSIT, d4),
                        Operation.create(40984L, BigDecimal.ZERO, OperationType.DEPOSIT, d4)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideOperationsForNotEquals")
    void should_validate_not_equal(Operation o1, Operation o2) {
        assertNotEquals(o1, o2);
    }

    private static Stream<Arguments> provideOperationsForNotEquals() throws AmountNegativeException {
        Instant d1 = Instant.ofEpochSecond(1424080802);
        Instant d2 = Instant.ofEpochSecond(1455616802);
        Instant d3 = Instant.ofEpochSecond(1487239202);
        Instant d4 = Instant.ofEpochSecond(1518775202);

        return Stream.of(
                Arguments.of(
                        Operation.create(123L, BigDecimal.ZERO, OperationType.DEPOSIT, d1),
                        Operation.create(45L, BigDecimal.ZERO, OperationType.DEPOSIT, d1)
                ),
                Arguments.of(
                        Operation.create(56L, BigDecimal.ZERO, OperationType.DEPOSIT, d2),
                        Operation.create(56L, BigDecimal.ONE, OperationType.DEPOSIT, d2)
                ),
                Arguments.of(
                        Operation.create(865L, BigDecimal.TEN, OperationType.DEPOSIT, d3),
                        Operation.create(865L, BigDecimal.ONE, OperationType.WITHDRAWAL, d3)
                ),
                Arguments.of(
                        Operation.create(40984L, BigDecimal.ZERO, OperationType.DEPOSIT, d4),
                        Operation.create(40984L, new BigDecimal(500), OperationType.DEPOSIT, d1)
                )
        );
    }


    @Test
    void should_throw_exception_when_amount_negative() {
        assertThrows(AmountNegativeException.class, () ->
                Operation.create(123L, new BigDecimal(-43), OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802))
        );
    }

    @ParameterizedTest
    @MethodSource("provideOperationsWithNullArguments")
    void should_throw_exception_when_null_argument_on_operation(Long accountID, BigDecimal amount, OperationType operationType, Instant date) {
        assertThrows(IllegalArgumentException.class, () ->
                Operation.create(accountID, amount, operationType, date)
        );
    }

    private static Stream<Arguments> provideOperationsWithNullArguments() {
        return Stream.of(
                Arguments.of(null, BigDecimal.ZERO, OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802)),
                Arguments.of(54L, null, OperationType.DEPOSIT, Instant.ofEpochSecond(1424080802)),
                Arguments.of(78L, BigDecimal.TEN, null, Instant.ofEpochSecond(1424080802)),
                Arguments.of(795L, new BigDecimal(366), OperationType.WITHDRAWAL, null)
        );
    }
}
